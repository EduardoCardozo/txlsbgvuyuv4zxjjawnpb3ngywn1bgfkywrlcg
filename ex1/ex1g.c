#include <stdio.h>
#include <stdlib.h>

#define TAM_STACK 100

typedef struct {
    char itens[TAM_STACK];
    int  top;
}stack;

void inicia_stack(stack *stack);
int vazio(stack *stack);
int cheio(stack *stack);
void push(stack *stack, char i);
char pop(stack *stack);
void printa_pilha(stack *stack);

void funcao_exercicio(stack *stack, char i);

int main(void){
    stack *pilha = (stack *)malloc(sizeof(stack));
    inicia_stack(pilha);
    /* Estado Inicial Da pilha */
    push(pilha, 'A');
    push(pilha, 'B');
    push(pilha, 'C');
    push(pilha, 'D');
    push(pilha, 'E');
    push(pilha, 'F');
    push(pilha, 'G');

    printf("Antes: ");
    printa_pilha(pilha);

    funcao_exercicio(pilha, 'I');

    printf("\n");

    printf("Depois: ");
    printa_pilha(pilha);
    printf("\n");

    return 0;
}

void funcao_exercicio(stack *stack, char i) {

    int total_itens = stack->top;
    int repete = total_itens - 2;

    char aux[stack->top + 1];

    for(int i = 0; i < repete; i++) {
        aux[i] = pop(stack);

    }

    push(stack, i);

    for(int i = repete - 1; i >= 0; i-- ) {
        push(stack, aux[i]);

    }


}

void inicia_stack(stack *stack) {
    stack->top = -1;
}

int vazio(stack *stack) {
    if(stack->top == -1) {
        return 1;
    }else{
        return 0;
    }

}

int cheio(stack *stack) {
    if(stack->top == TAM_STACK - 1) {
        return 1;
    }else{
        return 0;
    }

}

void push(stack *stack, char i) {
    if(!cheio(stack)) {
        stack->top++;
        stack->itens[stack->top] = i;
    }

}

char pop(stack *stack) {
    if(!vazio(stack)) {
        char aux;
        aux = stack->itens[stack->top];
        stack->top--;
        return aux;
    }

}

void printa_pilha(stack *stack) {

    int tam_pilha = stack->top + 1;
    char aux[tam_pilha];

    int counter = 0;

    while(!vazio(stack)) {
        aux[counter] = pop(stack);
        counter++;
    }
    
    for(int i = tam_pilha - 1; i >= 0; i--){
        printf("%c", aux[i]);
    }

    for(int i = tam_pilha; i >= 0; i--) {
        push(stack,aux[i]);
    }

}

