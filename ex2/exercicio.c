#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM_STACK 100
#define TAM_STR   5

typedef struct {
    char itens[TAM_STACK];
    int  top;
}stack;

void inicia_stack(stack *stack);
int vazio(stack *stack);
int cheio(stack *stack);
void push(stack *stack, char i);
char pop(stack *stack);
void printa_pilha(stack *stack);

int main(void){
    
    stack *pilha1 = (stack *)malloc(sizeof(stack));
    stack *pilha2 = (stack *)malloc(sizeof(stack));
    
    char string1[TAM_STR];

    int count = 0;
    
    inicia_stack(pilha1);
    inicia_stack(pilha2);

    printf("Digite a string: ");
    scanf("%s", string1);

    /* Carrega a string na pilha */

    for(int i=0; i<=strlen(string1); i++){
        push(pilha1, string1[i]);
    }

    /* Carrega a string na proxima pilha de maneira invertida */

    for(int i=strlen(string1); i >= 0;i-- ){
        push(pilha2, string1[i]);
    }
    
    printf("String X: ");
    printa_pilha(pilha1);
    printf("\n");
    
    printf("String Y: ");
    printa_pilha(pilha2);
    printf("\n");
    
    for(int i=0; i <= pilha1->top + 1; i++ ) {
        char aux, aux2;

        aux  = pop(pilha1);
        aux2 = pop(pilha2);
    
        if(aux == aux2) {
            count++;
        }

    }

    if(count == pilha1->top + 1) {
        printf("X C Y\n");
    }else{
        printf("X C/ Y\n");
    }


    return 0;
}


void inicia_stack(stack *stack) {
    stack->top = -1;
}

int vazio(stack *stack) {
    if(stack->top == -1) {
        return 1;
    }else{
        return 0;
    }

}

int cheio(stack *stack) {
    if(stack->top == TAM_STACK - 1) {
        return 1;
    }else{
        return 0;
    }

}

void push(stack *stack, char i) {
    if(!cheio(stack)) {
        stack->top++;
        stack->itens[stack->top] = i;
    }

}

char pop(stack *stack) {
    if(!vazio(stack)) {
        char aux;
        aux = stack->itens[stack->top];
        stack->top--;
        return aux;
    }

}

void printa_pilha(stack *stack) {
    int tam_pilha = stack->top + 1;
    char aux[tam_pilha];

    int counter = 0;

    while(!vazio(stack)) {
        aux[counter] = pop(stack);
        counter++;
    }
    
    for(int i = tam_pilha - 1; i >= 0; i--){
        printf("%c", aux[i]);
    }

    for(int i = tam_pilha; i >= 0; i--) {
        push(stack,aux[i]);
    }

}


