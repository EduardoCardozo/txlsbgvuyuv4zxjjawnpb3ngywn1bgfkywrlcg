#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM_STACK 100
#define TAM_STR   15

typedef struct {
    char itens[TAM_STACK];
    int  top;
}stack;

void inicia_stack(stack *stack);
int vazio(stack *stack);
int cheio(stack *stack);
void push(stack *stack, char i);
char pop(stack *stack);
void printa_pilha(stack *stack);
void esvazia_pilha(stack *stack);

void inverte_pilha(stack *pilha_x, stack *pilha_y);
int  realizaChecagem(stack *pilha_x, stack *pilha_y);

int main(void){
    
    /*
     * String A Ser checada
     */
    
    char string_entrada[] = "AAADBABDCCC";

    /* Realiza tratamento na string de entrada:
     *  
     * Como a chacagem é realizada a cada char 'D', essa parte irá
     * passar o nullbyte uma posição para a frente e, no lugar que
     * estava, irá adicionar um 'D'
     *  
     * */

    strcat(string_entrada, "D\0");

    /*
     * Inicia as pilhas que serão usadas
     */

    stack *pilha_x = (stack *)malloc(sizeof(stack));
    stack *pilha_y = (stack *)malloc(sizeof(stack));
    
    inicia_stack(pilha_x);
    inicia_stack(pilha_y);
   
    /*
     * O programa irá ler até encontrar um 'D', todo caractere até ele
     * irá ser empilhado na "pilha_x", assim que encontrar um 'D' ele chama
     * a função que irá tratar de checar se X está contido em Y
     */

    for(int i = 0; i <= strlen(string_entrada) - 1; i++) {
        if(string_entrada[i] != 'D') {
            push(pilha_x, string_entrada[i]);
        }else{
            // Caso a função retorne 1, significa que discrepancias foram encontradas
            if(realizaChecagem(pilha_x,pilha_y) == 1) { 
                printf("A string não obedece à regra!\n");
                exit(0);
            }else {
                printf("A string obedece à regra!\n");
                exit(0);

            }
        }
    }
    
    return 0;
}

int realizaChecagem(stack *pilha_x, stack *pilha_y) {

    int counter = 0; // Esse counter irá contar quantas discrepancias há em cada pedaço da string

    inverte_pilha(pilha_x, pilha_y); // Torna y o inverso de x

    // Checagem char por char
    for(int i = 0; i <= pilha_x->top + 1; i++) {
        if(!(pilha_x->itens[i] == pilha_y->itens[i])) {
            counter++;
        }
    }
    
    // Esvazia as pilhas, para que possam receber o proximo pedaço de string
    esvazia_pilha(pilha_x);
    esvazia_pilha(pilha_y);

    //Caso não haja discrepancias, retorna 0, do contrário, retorna 1
    if(counter == 0) {

        return 0;

    }else {

        return 1;

    }

    /*
     *
     * Haver discrepancias entre os dois pedaços de string significa que x C/ Y
     * 
     */

}

/*
 *
 * Torna a pilha_y o inverso da pilha_x
 *
 */

void inverte_pilha(stack *pilha_x, stack *pilha_y) {

    for(int i = pilha_x->top ; i >= 0; i--) {

        push(pilha_y, pilha_x->itens[i]);
        
    }
        
}

/*
 * 
 *
 * Funções Utilizadas Pra o controle das pilhas!
 *
 *
 */

void inicia_stack(stack *stack) {
    stack->top = -1;
}

int vazio(stack *stack) {
    if(stack->top == -1) {
        return 1;
    }else{
        return 0;
    }

}

int cheio(stack *stack) {
    if(stack->top == TAM_STACK - 1) {
        return 1;
    }else{
        return 0;
    }

}

void push(stack *stack, char i) {
    if(!cheio(stack)) {
        stack->top++;
        stack->itens[stack->top] = i;
    }

}

char pop(stack *stack) {
    if(!vazio(stack)) {
        char aux;
        aux = stack->itens[stack->top];
        stack->top--;
        return aux;
    }

}

void printa_pilha(stack *stack) {

    int tam_pilha = stack->top + 1;
    char aux[tam_pilha];

    int counter = 0;

    while(!vazio(stack)) {
        aux[counter] = pop(stack);
        counter++;
    }
    
    for(int i = tam_pilha - 1; i >= 0; i--){
        printf("%c", aux[i]);
    }

    for(int i = tam_pilha; i >= 0; i--) {
        push(stack,aux[i]);
    }

}

void esvazia_pilha(stack *stack) {

    while(!vazio(stack)) {
        pop(stack);
    }

}

