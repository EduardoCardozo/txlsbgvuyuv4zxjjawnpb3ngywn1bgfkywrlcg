#include <stdio.h>
#include <stdlib.h>

#define TAM_STACK 100

typedef struct {
    char itens[TAM_STACK];
    int  top;
}stack;

void inicia_stack(stack *stack);
int vazio(stack *stack);
int cheio(stack *stack);
void push(stack *stack, char i);
char pop(stack *stack);

void inicia_stack(stack *stack) {
    stack->top = -1;
}

int vazio(stack *stack) {
    if(stack->top == -1) {
        return 1;
    }else{
        return 0;
    }

}

int cheio(stack *stack) {
    if(stack->top == TAM_STACK - 1) {
        return 1;
    }else{
        return 0;
    }

}

void push(stack *stack, char i) {
    if(!cheio(stack)) {
        stack->top++;
        stack->itens[stack->top] = i;
    }else{
        printf("ERRO: A Pilha já está cheia!\n");
        exit(1);
    }

}

char pop(stack *stack) {
    if(!vazio(stack)) {
        char aux;
        aux = stack->itens[stack->top];
        stack->top--;
        return aux;
    }else{
        printf("ERRO: Não É possível retirar um item de uma pilha vazia!\n");
        exit(1);
    }

}

