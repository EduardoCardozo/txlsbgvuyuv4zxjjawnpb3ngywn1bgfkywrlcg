#include <stdio.h>
#include <stdlib.h>

#define TAM_STACK 100

typedef struct {
    int itens[TAM_STACK];
    int  top;
}stack;

void inicia_stack(stack *stack);
int vazio(stack *stack);
int cheio(stack *stack);
void push(stack *stack, char i);
char pop(stack *stack);
void printa_pilha(stack *stack);

void fatora(stack *stack, int num);

int main(void){
    
    int num = 1125;

    stack *divisores = (stack *)malloc(sizeof(stack));

    inicia_stack(divisores);

    fatora(divisores, num);

    printf("Numero %i fatorado: ", num);

    for(int i = 0; i < divisores->top + 1; i++){
        if(i == divisores->top){
            printf("%i ", divisores->itens[i]);
        }else{
            printf("%i x ", divisores->itens[i]);
        }
    }

    printf("\n");

    return 0;
}

void fatora(stack *stack, int num) { 
    // Divide o numero por dois até que ele se torne um impar
    while (num % 2 == 0) { 
        push(stack, 2);
        num = num / 2; 
    } 
    // Dcomeça a terstar divisores para aqule número
    for (int i = 3; i <= num; i = i+2) {
        while (num % i == 0){
            push(stack, i);
            num = num / i; 
        } 
    } 
    
    if (num > 2){
        push(stack, num);

    } 

} 

void inicia_stack(stack *stack) {
    stack->top = -1;
}

int vazio(stack *stack) {
    if(stack->top == -1) {
        return 1;
    }else{
        return 0;
    }

}

int cheio(stack *stack) {
    if(stack->top == TAM_STACK - 1) {
        return 1;
    }else{
        return 0;
    }

}

void push(stack *stack, char i) {
    if(!cheio(stack)) {
        stack->top++;
        stack->itens[stack->top] = i;
    }

}

char pop(stack *stack) {
    if(!vazio(stack)) {
        char aux;
        aux = stack->itens[stack->top];
        stack->top--;
        return aux;
    }

}

void printa_pilha(stack *stack) {

    int tam_pilha = stack->top + 1;
    char aux[tam_pilha];

    int counter = 0;

    while(!vazio(stack)) {
        aux[counter] = pop(stack);
        counter++;
    }
    
    for(int i = tam_pilha - 1; i >= 0; i--){
        printf("%i ", aux[i]);
    }

    for(int i = tam_pilha; i >= 0; i--) {
        push(stack,aux[i]);
    }

}
